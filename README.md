# SOGMA

**Simple Option Game Maker Advanced**, or **SOGMA** for short, is an "game engine" for making simple option-based text games.

## Running games

You need to have a `json` file with the game you want to play. Then, from SOGMA directory:

### Linux

```sh
./sogma path/to/game.json # Ommiting the parameter will default to 'game.json' file.
```

### Windows

```cmd
sogma.exe path\to\game.json
```

Dragging the file to SOGMA should work too (not tested).

## Making games

Read [the wiki](https://gitlab.com/grzesiek11/sogma/-/wikis/Home).